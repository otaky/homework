"use strict";

const PrimeFactors = require("./PrimeFactors.js");

describe( "PrimeFactors", () =>{

    const PRIMEFACTORS = new PrimeFactors();

    describe( "generate возвращает простые множители", () =>{

        it('1 должен вернуть []', () => {
            expect(PRIMEFACTORS.generate(1)).toEqual([]);
        });

        it('2 должен вернуть [2]', () => {
            expect(PRIMEFACTORS.generate(2)).toEqual([2]);
        });

        it('3 должен вернуть [3]', () => {
            expect(PRIMEFACTORS.generate(3)).toEqual([3]);
        });

        it('4 должен вернуть [2,2]', () => {
            expect(PRIMEFACTORS.generate(4)).toEqual([2,2]);
        });

        it('5 должен вернуть [5]', () => {
            expect(PRIMEFACTORS.generate(5)).toEqual([5]);
        });

        it('6 должен вернуть [2.3]', () => {
            expect(PRIMEFACTORS.generate(6)).toEqual([2,3]);
        });

        it('7 должен вернуть [7]', () => {
            expect(PRIMEFACTORS.generate(7)).toEqual([7]);
        });

        it('8 должен вернуть [2.2.2]', () => {
            expect(PRIMEFACTORS.generate(8)).toEqual([2,2,2]);
        });

        it('9 должен вернуть [3,3]', () => {
            expect(PRIMEFACTORS.generate(9)).toEqual([3,3]);
        });

        it('10 должен вернуть [2,5]', () => {
            expect(PRIMEFACTORS.generate(10)).toEqual([2,5]);
        });

        it('12 должен вернуть [2,2,3]', () => {
            expect(PRIMEFACTORS.generate(12)).toEqual([2,2,3]);
        });

        it('мегачисло 4620 вернет [2, 2, 3, 5, 7, 11] ); ', () => {
            expect(PRIMEFACTORS.generate(4620)).toEqual([2, 2, 3, 5, 7, 11]);
        });


    });
});