"use strict";

class BowlingGame {

    constructor() {
        this.rolls = [];
    }

    roll(pins) {
        this.rolls.push(pins);

    }

    getScore(){
        let score, numberOfRoll;

        score = 0;
        numberOfRoll = 0;

        //в игре 10 раундов
        for (let gameRound = 0; gameRound < 10; gameRound++) {

            // это Strike ?
            if( this.isStrike(numberOfRoll) ){
                score = score + 10 + this.strikeBonus(numberOfRoll);
                numberOfRoll++ ;
            }

            // это Spare ?
            else if (this.isSpare(numberOfRoll)) {
                score = score + 10 + this.spareBonus(numberOfRoll);
                numberOfRoll = numberOfRoll + 2;
            }

            // если не Strike и не  Spare то просто считаем сумму
            else {
                score = score + this.rolls[numberOfRoll] + this.rolls[numberOfRoll + 1];
                numberOfRoll = numberOfRoll + 2;
            }

        }
        return score;
    }

    //здесь определение всех првоерок на бонусы

    // если бросок = 10 это Strike!
    isStrike(numberOfRoll) {
        return this.rolls[numberOfRoll] === 10;
    }

    // бонус страйка это сумма двух следующих бросков
    strikeBonus(numberOfRoll){
        return this.rolls[numberOfRoll +1] + this.rolls[numberOfRoll +2] ;
    }

    // если сумма бросков === 10 это Spare , значит нужен бону
    isSpare(numberOfRoll) {
        return this.rolls[numberOfRoll] + this.rolls[numberOfRoll + 1] === 10;
    }

    // бонус = 10 + следующий бросок
    spareBonus(numberOfRoll) {
        return this.rolls[numberOfRoll + 2];
    }

}

module.exports = BowlingGame;