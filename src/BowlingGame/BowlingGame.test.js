"use strict";

const BowlingGame  = require('./BowlingGame');

describe('Игра в боулинг !', () => {

    let game;

    beforeEach(() => {
        game = new BowlingGame();
    });

    it('0 сбитых = 0 очков', () => {
        rollMany(20, 0);
        expect(game.getScore()).toEqual(0);
    });

    it('очки = сумма сбитых кеглей', () => {
        rollMany(20, 1);
        expect(game.getScore()).toEqual(20);
    });

    it('считает бонус за Spare как 10 + след.бросок', () => {
        rollSpare();
        game.roll(3);
        rollMany(17, 0);
        expect(game.getScore()).toEqual(16);
    });

    it('считает бонус за страйкстрайк', () => {
        rollStrike();
        game.roll(3);
        game.roll(4);
        rollMany(16, 0);
        expect(game.getScore()).toEqual(24);
    });


    function rollMany(n, pins) {
        for (let i = 0; i < n; i++) {
            game.roll(pins);
        }
    }

    function rollSpare() {
        game.roll(5);
        game.roll(5);
    }

    function rollStrike() {
        game.roll(10);
    }

});