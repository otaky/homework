"use strict";

const StringCalculator = require("./StringCalculator.js");

describe("Строковый калькулятор",() => {

    let calc;

    beforeEach(() => {
        calc = new StringCalculator();
    });

        it("Пустая строка возвращает => `0` ", () => {
            expect( calc.add("") ).toEqual(0);
        });

        it("одиночная строка возвращает число", () => {
            expect( calc.add("1")).toEqual(1);
        });

        it("возвращает сумму двух чисел", () => {
            expect( calc.add("1,1")).toEqual(2);
        });

        it('числа больше 1000 игнорируются', () => {
            expect(calc.add('1,2\n3,4,1000,1001')).toEqual(1010);
        });

        it('перенос строки возвращает сумму 2 чисел', () => {
            expect(calc.add('1\n2')).toEqual(3);
        });


        it('перенос строки возвращает сумму 3 чисел', () => {
            expect(calc.add('1\n2,1')).toEqual(4);
        });

        it('отрицательцые числа не разрешены', () => {
            expect(() => calc.add('-1,2,-3')).toThrow(new Error('отрицательные числа не разрешены: -1,-3'));
        });

        it('числа больще 1000 игнорируются', () => {
            expect(calc.add('1,2\n3,4,1000,1001')).toEqual(1010);
        });

        it('символ переденый после // используется как разделитель', () => {
            expect(calc.add('//#\n1#2')).toEqual(3);
        });

        it('множественные символы переданые после // используются как разделитель', () => {
            expect(calc.add('//xyz\n1xyz2')).toEqual(3);
        });

});