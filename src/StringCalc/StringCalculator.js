"use strict";

class StringCalculator {

    add(numberString) {

        let result, separator, indexOfNewLine;
        let numbers, negativeNumbers = [];

        returnZeroIfNumberStringEmpty();
        convertStringIntoNumber();
        replaceAndSplitNumberString();
        convertAllEliementInArrayIntoNumbers();

        throwNewErrorIfFindNegativeNumbers();
        ignoreNumbersThatMoreThan1000();
        getSumm();

        return result;


        // начало блока с проверками :

        function returnZeroIfNumberStringEmpty() {
            if( !numberString ){
                return 0;
            }
        }

        function replaceAndSplitNumberString() {
            // все что от // и до \n это делитель !
            if (numberString.startsWith("//") ) {
                // ищем \n
                indexOfNewLine = numberString.indexOf('\n');
                //взятие подстроки, начало с 2 тк(0 и 1 это "//" в сроке , конец это перенос строки)
                separator = numberString.substring(2, indexOfNewLine);
                //ВСЕ найденые переносы меняем на разделитель , и разбиваем на массив по разделителю
                numbers = numberString.replace(/\n/g, separator).split(separator);
            }
            else {
                numbers = numberString.replace(/\n/g, ",").split(",");
            }
        }

        function convertStringIntoNumber() {
            if( !isNaN(numberString) ){
                return result = +numberString;
            }
        }

        function convertAllEliementInArrayIntoNumbers() {
            for (let i =0; i < numbers.length; i++){
                numbers[i] = +numbers[i];
            }
        }

        function throwNewErrorIfFindNegativeNumbers() {
            negativeNumbers = numbers.filter(function (numbers) {//фильтруем, записываем отрицательное
                return numbers < 0;
            });

            if (negativeNumbers.length > 0) {
                throw new Error(`отрицательные числа не разрешены: ${negativeNumbers}`);
            }
        }

        function ignoreNumbersThatMoreThan1000() {
            numbers = numbers.filter(function (numbers) {
                return numbers <= 1000;
            });
        }

        function getSumm() {
            return numbers.reduce(function (sum, current) {
                return result = ( sum + current);
            });
        }

        //конец првоерок

    }

}

module.exports = StringCalculator;

