"use strict";

const Greeter = require('./Greeter');

describe('Greeter', () => {

    let date, logger, uut;
    date = new Date();
    date.setHours(14);
    date.setMinutes(0);
    logger = { log: jest.fn() };

    //где uut - вызов класса Greet у которого есть конструктор логгер и getTime
    uut = new Greeter(() => date.getTime(), logger);

    it('Здорововается по имени', () => {
        expect(uut.greet('Паша')).toEqual('Паша привет!');
        expect(uut.greet('Отаку')).toEqual('Отаку привет!');
    });

    it("обрезает пробелы", () => {
        expect(uut.greet("  Мистер")).toEqual("Мистер привет!");
        expect(uut.greet(" Мистер ")).toEqual("Мистер привет!");

        //	\t	Перемещает позицию печати к следующей позиции горизонтальной табуляции.
        //\n	Перемещает позицию печати на одну строку вниз (исходно — без возврата каретки) . Разделяет строки текстовых файлов в Unix-системах.
        expect(uut.greet(" Мистер\t\n ")).toEqual("Мистер привет!");

    });

    it("Делает первую букву имени заглавной" , () => {
        expect( uut.greet("господин")).toEqual("Господин привет!");
    });

    it("говорит доброе утро! с 6:00 до 12:00 утра", () =>{
        date.setHours(6);
        date.setMinutes(0);
        expect(uut.greet('Блять')).toEqual('Блять доброе утро!');

        date.setHours(7);
        date.setMinutes(0);
        expect(uut.greet('Блять')).toEqual('Блять доброе утро!');

        date.setHours(11);
        date.setMinutes(59);
        expect(uut.greet('Блять')).toEqual('Блять доброе утро!');
    });

    it('говорит добрый вечер с  18 до 22', () => {
        date.setHours(18);
        date.setMinutes(0);
        expect(uut.greet('Питрович')).toEqual('Питрович добрый вечер!');
        date.setHours(21);
        date.setMinutes(59);
        expect(uut.greet('Питрович')).toEqual('Питрович добрый вечер!');
    });

    it ("говорит доброй ночи! с 22 - 06 ", () =>{
        date.setHours(22);
        date.setMinutes(0);
        expect(uut.greet("Влад")).toEqual("Влад доброй ночи!");

        date.setHours(5);
        date.setMinutes(59);
        expect(uut.greet("Влад")).toEqual("Влад доброй ночи!");
    });

    it('простой вызов метода не записывает', () => {
        expect(logger.log).not.toHaveBeenCalled(); //простой вызов не записывается в логгер
    });

    it('записывает каждый вызов и всё так же приветствует', () => {
        uut.greet('гэндальф');
        expect(logger.log).toHaveBeenCalledTimes(1);
        expect(logger.log).toHaveBeenCalledWith(`Гэндальф привет!`);
    });

});
