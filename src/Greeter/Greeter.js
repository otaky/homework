'use strict';

class Greeter {
    constructor( givenDate, logger) {
        this.givenDate = givenDate; //сохранили переданное число
        this.logger = logger;//сохраним перенный  по аналогии с датой обьект
    }

    greet(name) {//метод greet

        //переменные которые нам понадобятся для операций
        let greetingWord, getTrimmedName, getFirstLetterUpper, currentDate, getHourseFromCurrentDate, theName;

        //обработаем имя для тестов
        //обрежем пробелы
        //работа со строкой, сохранить строку, отредактированть , записать значение
        //первая буква заглавная + вернуть остаток строки
        getTrimmedName = name.trim();
        getFirstLetterUpper = getTrimmedName[0].toUpperCase() + getTrimmedName.slice(1);

        //тоесть тут уже обработанное имя ,
        theName = getFirstLetterUpper;

        //формируем дату из цифр переданых в конструктор для дальнейщего сравнения "который час?"
        //те получили цифры, вернули час
        currentDate = new Date(this.givenDate());
        getHourseFromCurrentDate = currentDate.getHours();

        // 7. `greet` logs into console each time it is called ???
        //еще надо логи записать, предполагается что в конструктор передается аргумент ( обьект с методом .log ) это в тестах
        //метод log записывает в обьект каждый свой вызов
        this.logger.log(`${theName} привет!`);

        // условие с( 22 до 6 ) сравнение не пройдёт, перераспределим время чтоб можно было сравнивать
        // определим 3 отрезка c 6-12 (утреннее)  c 12-18 (обычный привет) c 18-22 (вечер) , а доброй ночи если предыдушие три условия не выполнились
        if ( getHourseFromCurrentDate >= 6 && getHourseFromCurrentDate < 12 ) {
            greetingWord = `доброе утро!`;
        }
        else if ( getHourseFromCurrentDate >= 12 && getHourseFromCurrentDate < 18 ) {
            greetingWord = `привет!`;
        }
        else if ( getHourseFromCurrentDate >= 18 && getHourseFromCurrentDate < 22) {
            greetingWord = `добрый вечер!`;
        }

        else { greetingWord = `доброй ночи!`}

        // я щерстяной волчара, боже как я хорош, как мощьна моя шаблонная строка
        return ( `${theName} ${greetingWord}` );
    }
}

module.exports = Greeter;